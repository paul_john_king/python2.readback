#!/usr/bin/env python2

# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# Stolen shamelessly from
# https://stackoverflow.com/questions/2301789/read-a-file-in-reverse-order-using-python.

import os

class ReverseFile:

	def __init__(self, path):
		self.__path = path

	def __enter__(self):
		self.__file = open(self.__path, 'r')
		self.__file.seek(0, os.SEEK_END)
		self.__size = self.__file.tell()
		self.__offset = 1
		if self.__size > 0 and self.__char(1) == '\n':
			self.__offset = 2
		return self

	def __exit__(self, type, value, traceback):
		self.__file.close()

	def __char(self, offset):
		self.__file.seek(self.__size - offset)
		return self.__file.read(1)

	def __iter__(self):
		return self

	def next(self):
		if self.__offset > self.__size:
			raise StopIteration
		line = '\n'
		while self.__offset <= self.__size:
			char = self.__char(self.__offset)
			self.__offset += 1
			if char == '\n':
				break
			line = char + line
		return line

if __name__ == "__main__":
	with ReverseFile('foo') as file:
		for line in file:
			print('['+line+']')
	print('----')
	with ReverseFile('bar') as file:
		for line in file:
			print('['+line+']')
	print('----')
	with ReverseFile('file') as file:
		for line in file:
			print('['+line+']')
	print('----')
